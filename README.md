[phue]: https://github.com/studioimaginaire/phue
[NLTK]: http://www.nltk.org/
[Bitbucket Wiki]: https://bitbucket.org/Wisellama/cs164final/wiki/Home

# Illum

A natural language (NLP) interface for the Philips Hue brand of smart
light bulbs with the intent of being extended to home automation in
general.

### Requirements

* Python 3
* [NLTK][]
* [phue][]

### Running

For testing without the physical lights/base station, run
`./test.py`. To actually run it with the lights, look at `./demo.py`
for an example of how to set it up currently. You can define people
with `Human` objects, give them possessions (so the lights belong to
someone), and finally ask for input from the terminal.

### Other

You can find links to our original design documents and such on our
[Bitbucket Wiki][]. This project was originally created for our UC
Berkeley CS 164 class on programming languages and compilers.

# PA6

This was an assignment to prepare us for our final project, Illum. PA6
parsed dates and times using Python NLTK. 
