#!/usr/bin/env python3

# NOTE: Make sure you have nltk and its data installed
# http://www.nltk.org/data.html

import calendar
import sys
import re
from datetime import datetime, date, time, timedelta
from pprint import pprint
import nltk

WEEKS_PER_YEAR = 52.17857142857143;

NUMBERS = {'zero':'0', 'one':'1', 'two':'2', 'three':'3', 'four':'4', 'five':'5',
        'six':'6', 'seven':'7', 'eight':'8', 'nine':'9', 'ten':'10'}

# Our custom tags which allowed us to only focus on date/time related text
CUSTOM_TAGS = [
        (r'sec(ond)?s?','SECS'),
        (r'min(ute)?s?','MINS'),
        (r'h(ou)?rs?','HOURS'),
        (r'days?','DAYS'),
        (r'w(ee)?ks?','WEEKS'),
        (r'mo(nth)?s?','MONTHS'),
        (r'y(ea)?rs?','YEARS'),
        (r'decades?', 'DECADES'),
        (r'centur(y|ies)', 'CENTS'),
        (r'millenn(ium|ia)', 'MILL'),
        (r'^(0?[1-9]|1[0-2]):[0-5][0-9]$', '12HR-TIME'),
        (r'^([0-1]?[0-9]|2[0-4]):[0-5][0-9]$', '24HR-TIME'),
        (r'am|pm', 'MERIDIEM'),
        (r'o\'clock', 'OCLOCK'),
        (r'midnight|noon', 'MIDNOON'),
        (r'yesterday|today|tomorrow', 'YTT'),
        (r'now', 'NOW'),
        (r'jan(uary)?|feb(ruary)?|mar(ch)?|apr(il)?|may|june?|jul(y)?|aug(ust)?'
         r'|sept?(ember)?|oct(ober)?|nov(ember)?|dec(ember)?', 'MONTH'),
        (r'^(sun(day)?|mon(day)?|tue?s?(day)?|wed(nesday)?|thu?r?s?(day)?'
         r'|fri(day)?|sat(urday)?)$', 'WK-DAY'),
        (r'b?c\.?e|b\.?c|a\.?d', 'ERA'),
        (r'[0-9]*(1[0-9]th|[04-9]th|1st|2nd|3rd)','ORDINAL'),
        (r'^[0-9]+$', 'NUM'),
        (r',$', ','),
        (r'[\.;!?]', 'PUNCT'),
        (r'/', '/'),
        (r'-', '-'),
        (r'on', 'ON'),
        (r'by', 'BY'),
        (r'the', 'THE'),
        (r'of', 'OF'),
        (r'in', 'IN'),
        (r'at', 'AT'),
        (r'from', 'FROM'),
        (r'.*', 'IGNORE')
]

"""
Our grammar for chunking the tagged tokens into meaningful representations of
time and dates, as well as other necesary tokens like duration.
"""
GRAMMAR = r"""
        UNITS:    {<SECS|MINS|HOUR|DAYS|WEEKS|MONTHS|YEARS|DECADES|CENTS|MILL>}
        DURATION: {<NUM> <UNITS>}
        TIME:     {<AT>? <NUM|12HR-TIME> <MERIDIEM>}
                  {<AT>? <NUM> <OCLOCK>}
                  {<AT>? <MIDNOON>}
                  {<AT>? <12HR-TIME|24HR-TIME> <OCLOCK>?}
        DATE:     {<BY>? <YTT>}
                  {<ON>? <WK-DAY>}
                  {<NUM> <-|/> <NUM> <-/> <NUM>}
                  {<ON>? <THE>? <ORDINAL> <OF> <MONTH> <,>? <NUM>?}
                  {<ON>? <MONTH> <NUM|ORDINAL> <,>? <NUM>?}
        RIGHT-NOW:{<NOW>}
        DATETIME: {<IN>? <DURATION> (<FROM> <DATE|RIGHT-NOW>)?}
"""

def extract_datetime(sentence):
    """Our starter program. You can see the entire skeleton of the program:
        tokenize -> light desugaring -> tagging -> parsing -> interpreting"""
    tokens = tokenize(sentence)
    desugar_numbers(tokens)
    tagger = nltk.RegexpTagger(CUSTOM_TAGS)
    tagged = tagger.tag(tokens)
    parser = nltk.RegexpParser(GRAMMAR)
    parsed = parser.parse(tagged)
    return interpret(parsed)

def tokenize(sentence):
    """the default tokenizer did not seperate am's and pm's from numbers(7pm)"""
    sentence = re.sub(r'([0-9])[AaPp][Mm]', r'\1 pm', sentence)
    return [word.lower() for word in nltk.word_tokenize(sentence)]

def desugar_numbers(tokens):
    """Allows for simpler grammars/tags by converting word numbers to ints"""
    for i,token in enumerate(tokens,start=0):
        if token in NUMBERS:
            tokens[i] = NUMBERS[token]

def interpret(tree):
    """Tree's basically represent non terminals. Read the repsonse for my less
    than pleased opinion of it """
    if tree.label() == 'UNITS':
        return tree[0][1]
    if tree.label() == 'DURATION':
        return get_timedelta(tree)
    if tree.label() == 'TIME':
        return get_time(tree)
    if tree.label() == 'DATE':
        return get_date(tree)
    if tree.label() == 'RIGHT-NOW':
        return datetime.now()
    if tree.label() == 'DATETIME':
        return get_relative_datetime(tree)

    dates_times = {}
    for child in tree:
        if isinstance(child, nltk.tree.Tree):
            result = interpret(child)
            dates_times[child.label()] = result

    """
    Many dates and time may come out of the interpreting. We use a dictionary to
    restrict one object per classification (date, time, and datetime).
    After this, we must decide how to combine them.
    """
    dt = dates_times.get('DATETIME', None)
    d = dates_times.get('DATE', None)
    t = dates_times.get('TIME', None)
    if dt is not None:
        if t is not None:
            return dt.combine(dt.date(), t)
        else:
            return dt
    elif d is not None:
        if t is not None:
            return datetime.combine(d, t)
        else:
            return datetime.combine(d, datetime.min.time())
    elif t is not None:
        return datetime.combine(date.today(), t)
    else:
        return None

def get_int(num):
    """Helper function to extract number from 10's and 10th's"""
    number, tag = num
    if tag == 'ORDINAL':
        return int(number[:-2])
    else:
        return int(number)

def get_timedelta(duration):
    """Takes in duration Tree object and returns a timedelta"""
    num, units = duration
    amount = get_int(num)
    unit_tag = units[0][1]
    if unit_tag == 'SECS':
        return timedelta(seconds=amount)
    elif unit_tag == 'MINS':
        return timedelta(minutes=amount)
    elif unit_tag == 'HOURS':
        return timedelta(hours=amount)
    elif unit_tag == 'DAYS':
        return timedelta(days=amount)
    elif unit_tag == 'WEEKS':
        return timedelta(weeks=amount)
    elif unit_tag == 'MONTHS':
        return timedelta(days=30*amount)
    elif unit_tag == 'YEARS':
        return timedelta(weeks=WEEKS_PER_YEAR*amount)
    elif unit_tag == 'DECADES':
        return timedelta(weeks=WEEKS_PER_YEAR*10*amount)
    elif unit_tag == 'CENTS':
        return timedelta(weeks=WEEKS_PER_YEAR*100*amount)
    elif unit_tag == 'MILL':
        return timedelta(weeks=WEEKS_PER_YEAR*1000*amount)
    else:
        raise Exception('Incorrect durration.')

def get_time(tree):
    """Takes in a tree time object and returns a datetime.time object"""
    children = {value: key for key, value in tree}
    if 'MERIDIEM' in children:
        if 'NUM' in children:
            hour = int(children['NUM']) % 13
            time_str = '{:0>2}:00 {}'.format(hour, children['MERIDIEM'])
        else:
            value = children['12HR-TIME']
            time_str = '{:0>5} {}'.format(value, children['MERIDIEM'])
        return datetime.strptime(time_str, '%I:%M %p').time()
    elif 'NUM' in children:
        hour = int(children['NUM'])
        if datetime.now().hour >= hour:
            hour = min(hour+12, 23.99)
        return time(hour)
    elif 'MIDNOON' in children:
        value = children['MIDNOON']
        if value == 'midnight':
            return time(0)
        elif value == 'noon':
            return time(12)
        else:
            raise Exception('Incorrect MIDNOON')
    else:
        try:
            value = children['12HR-TIME']
        except:
            value = children['24HR-TIME']
        hour, minute = value.split(':')
        return time(int(hour), int(minute))

def get_date(tree):
    """Takes in date tree object returns a datetime.date object"""
    better_tree = [child for child in tree if child[1] not in ('AT', 'ON',
        'BY', 'THE', 'OF', ',')]
    current = date.today()
    if better_tree[0][1] == 'YTT':
        value = better_tree[0][0]
        if value == 'yesterday':
            return current - timedelta(days=1)
        elif value == 'today':
            return current
        else:
            return current + timedelta(days=1)
    elif better_tree[0][1] == 'WK-DAY':
        wk_days = {v.lower(): k for k, v in enumerate(calendar.day_abbr)}
        abbr = better_tree[0][0][:3].lower()
        dates_wk = wk_days[abbr]
        offset = (dates_wk - current.weekday()) % 7
        return current + timedelta(days=offset)
    elif better_tree[0][1] == 'NUM':
        first = int(better[0][0])
        second = int(better[2][0])
        third = int(better[4][0])
        if first > 32:
            return date(first, second, third)
        elif second > 12 or first <= 12:
            return date(third, first, second)
        else:
            return date(third, second, first)
    else:
        if better_tree[0][1] == 'MONTH':
            month = better_tree[0][0]
            day = get_int(better_tree[1])
        elif better[0][1] == 'ORDINAL':
            day = get_int(better[0])
            month = better_tree[0][0]
        if len(better_tree) > 2:
            year = get_int(better_tree[2])
            if 100 > year:
                if year > 50:
                    year = '19' + str(year)
                else:
                    year = '20' + str(year)
        else:
            year = current.year
        date_str = '{} {:0>2} {:2>4}'.format(month[:3], day, year)
        return datetime.strptime(date_str, '%b %d %Y').date()

def get_relative_datetime(tree):
    """Used for phrases like 'in 10 hours' """
    min_time = datetime.min.time()
    if len(tree) in (1, 2):
        return datetime.now() + interpret(tree[-1])
    elif len(tree) == 3:
        return interpret(tree[0]) + datetime.combine(interpret(tree[-1]), min_time)
    else:
        return interpret(tree[1]) + datetime.combine(interpret(tree[-1]), min_time)

if __name__ == '__main__':
    sentence = 'On December 21st, 2014 seven pm, we should go hiking.'
    if len(sys.argv) > 1:
        sentence = ' '.join(sys.argv[1:])
    dt = extract_datetime(sentence)
    pprint(dt)
