import phue
from phue import Bridge
from phue import Light
from illum.test_light import TestLight

class TestBridge(Bridge):

    def __init__(self, username=None, config_file_path=None):
        self.ip = '0.0.0.0'
        self.username = username
        self.lights_by_id = {}
        self.lights_by_name = {}
        self._name = None

    def get_light_objects(self, mode='list'):
        if self.lights_by_id == {}:
            lights = []
            for i in range(1,4):
                lights.append(i)
            for light in lights:
                self.lights_by_id[int(light)] = TestLight(self, int(light))
                self.lights_by_name = self.lights_by_id
        return super().get_light_objects(mode)
            
    
