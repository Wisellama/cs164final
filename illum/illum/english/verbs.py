from illum.english.adjectives import Adjective
from illum.world import Action, WorldObject

class Verb:
    present = ()
    imperative = ()
    definition = None
    def __init__(self, subject, direct_object=None, indirect_object=None,
            adverb=None):
        self.subject = subject
        self.direct_object = direct_object
        self.indirect_object = indirect_object
        self.adverb = adverb

        self.discover_target()
        self.discover_definition()

    def discover_target(self):
        if self.direct_object is not None:
            self.target = self.direct_object
        else:
            self.target = self.subject

    def discover_definition(self):
        if self.adverb is not None and type(self) in self.adverb.definition:
            self.definition = self.adverb.definition[type(self)]

    #def materialize(self): # Future name
    def perform(self):
        if isinstance(self.definition, Action):
            goal = self.definition.end_state
        elif isinstance(self.definition, WorldObject):
            goal = self.definition
        else:
            raise Exception('Unknown goal.')

        if isinstance(self.target, list):
            objs = self.target
        else:
            objs = [self.target]

        for obj in objs:
            for key, val in goal.items():
                obj[key] = val

    def validate(self):
        if isinstance(self.definition, Action):
            compare = self.definition.end_state
        elif isinstance(self.definition, WorldObject):
            compare = self.definition
        else:
            raise Exception('Unknown compare.')

        if isinstance(self.target, list):
            objs = self.target
        else:
            objs = [self.target]

        for obj in objs:
            if not isinstance(obj, type(compare)):
                return False
            for key, val in compare.items():
                if key not in obj:
                    return None
                if obj[key] != val:
                    return False
        return True

class Be(Verb):
    present = ('am', 'are', 'is')
    imperative = ('be',)
    definition = WorldObject()

    def discover_definition(self):
        if isinstance(self.direct_object, Adjective):
            self.definition = self.direct_object.definition
            self.target = self.subject
        Verb.discover_definition(self)

class Dim(Verb):
    present = ('dim',)
    imperative = ('dim',)
    definition = WorldObject(brightness=50) # specific to bulbs

class Max(Verb):
    present = ('max',)
    imperative = ('max',)
    definition = WorldObject(brightness=254) # specific to bulbs

class Turn(Verb):
    present = ('turn', 'turns')
    imperative = ('turn',)

class Make(Verb):
    present = ('make', 'makes')
    imperative = ('make',)
