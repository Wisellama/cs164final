from illum.world import Action, WorldObject
from illum.english.verbs import Turn, Make, Be

class Adverb:
    def __init__(self, definition, positive, comparative=None, superlative=None):
        self.definition = definition
        self.positive = positive
        if comparative is None:
            letter = positive[-1]
            if letter == 'e':
                self.comparative = positive + 'r'
                self.superlative = positive + 'st'
            elif letter == 'y':
                self.comparative = positive[:-1] + 'ier'
                self.superlative = positive[:-1] + 'iest'
            else:
                self.comparative = positive + 'er'
                self.superlative = positive + 'est'
        else:
            self.comparative = comparative
            self.superlative = superlative

on = Adverb({
        Turn: Action(WorldObject(on=False), WorldObject(on=True)),
        Make: Action(WorldObject(on=False), WorldObject(on=True)),
        Be: WorldObject(on=True),
        }, 'on', 'more on', 'most on')

off = Adverb({
        Turn: Action(WorldObject(on=True), WorldObject(on=False)),
        Make: Action(WorldObject(on=True), WorldObject(on=False)),
        Be: WorldObject(on=False),
        }, 'off', 'more off', 'most off')

red = Adverb({
        Turn: WorldObject(hue=0),
        Make: WorldObject(hue=0),
}, 'red', 'more red', 'most red')

green = Adverb({
        Turn: WorldObject(xy=[.1,.75]),
        Make: WorldObject(xy=[.1,.75]),
}, 'green', 'more green', 'most green')

blue = Adverb({
        Turn: WorldObject(xy=[.1,.2]),
        Make: WorldObject(xy=[.1,.2]),
}, 'blue', 'more blue', 'most blue')

yellow = Adverb({
        Turn: WorldObject(xy=[.5,.5]),
        Make: WorldObject(xy=[.5,.5]),
}, 'yellow', 'more yellow', 'most yellow')

white = Adverb({
        Turn: WorldObject(xy=[.3,.3],saturation=254),
        Make: WorldObject(xy=[.3,.3],saturation=254),
}, 'white', 'more white', 'most white')
