from illum.world import WorldObject

class Adjective:
    def __init__(self, definition, positive, comparative=None, superlative=None):
        self.definition = definition
        self.positive = positive
        if comparative is None:
            letter = positive[-1]
            if letter == 'e':
                self.comparative = positive + 'r'
                self.superlative = positive + 'st'
            elif letter == 'y':
                self.comparative = positive[:-1] + 'ier'
                self.superlative = positive[:-1] + 'iest'
            else:
                self.comparative = positive + 'er'
                self.superlative = positive + 'est'
        else:
            self.comparative = comparative
            self.superlative = superlative

alive = Adjective(WorldObject(alive=True), 'alive', 'more alive', 'most alive')
dead = Adjective(WorldObject(alive=False), 'dead', 'more dead', 'most dead')

bed = Adjective(WorldObject(location='bed'), 'bed', 'more bed', 'most bed')
chair = Adjective(WorldObject(location='chair'), 'chair', 'more chair', 'most chair')
desk = Adjective(WorldObject(location='desk'), 'desk', 'more desk', 'most desk')
