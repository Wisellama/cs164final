from illum import Conversation
from illum.world import Human

class ConvoDict(dict):
    def __init__(self, illum):
        self.illum = illum

    def __missing__(self, key):
        if isinstance(key, Human):
            convo = Conversation(self.illum, key)
            self[key] = convo
            return convo
        else:
            raise KeyError('Non-human key')
