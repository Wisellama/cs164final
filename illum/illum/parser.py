import logging
import os
import re
from collections import defaultdict

from nltk import grammar, parse
from nltk.tokenize import sent_tokenize

from illum.contractions import contractions

from illum.english.adjectives import *
from illum.english.adverbs import *
from illum.english.verbs import *

class MissingGrammar(Exception): pass

class Parser:
    def __init__(self, generate=True):
        self.log = logging.getLogger('Parser')
        self.log.setLevel(logging.DEBUG)

        self.verb_table = {}
        self.adv_table = {}
        self.adj_table = {}
        self.additional_productions = defaultdict(set)
        self.learn_vocabulary()
        self.chart_parser = None
        if generate:
            self.generate_fcfg()

    def generate_fcfg(self):
        grammar_file = os.path.join(os.path.dirname(__file__), 'grammar.fcfg')
        with open(grammar_file) as fcfg_file:
            grammar_str = fcfg_file.read()
        for symbol, terms in self.additional_productions.items():
            terms = ['" "'.join(term.split()) for term in terms]
            production = '\n{} -> "{}"'.format(symbol, '" | "'.join(terms))
            self.log.debug('NEW PRODUCTION: ' + production)
            grammar_str += production
        feature_grammar = grammar.FeatureGrammar.fromstring(grammar_str)
        self.chart_parser = parse.FeatureEarleyChartParser(feature_grammar)

    # Future: change so it just imports "all" (look into .modules and globals)
    def learn_vocabulary(self):
        adjs = []
        for adj in (bed, desk, chair, alive, dead):
            adjs.append(adj.positive)
            self.adj_table[adj.positive] = adj
        self.add_production('ADJ', adjs)
        advs = []
        for adv in (on, off, red, white, green, blue, yellow):
            advs.append(adv.positive)
            self.adv_table[adv.positive] = adv
        self.add_production('ADV', advs)
        present = []
        imperative = []
        for verb in (Be, Make, Turn, Dim, Max):
            present_verbs = verb.present
            imperative_verbs = verb.imperative
            for word in present_verbs + imperative_verbs:
                self.verb_table[word] = verb
            present.extend(present_verbs)
            imperative.extend(imperative_verbs)
        self.add_production('V[-imperative]', present)
        self.add_production('V[+imperative]', imperative)

    def add_production(self, symbol, terms, generate=False):
        self.additional_productions[symbol] |= set(terms)
        if generate:
            self.generate_fcfg()

    def parse(self, raw_str):
        if self.chart_parser is None:
            raise MissingGrammar
        answers = []
        sentences = sent_tokenize(raw_str)
        for sentence in sentences:
            desugared_str = self.desugar(sentence)
            tokens = self.tokenize(desugared_str)
            try:
                trees = list(self.chart_parser.parse(tokens))
            except ValueError as e:
                self.log.warning(e)
                trees = []
            trees_len = len(trees)
            if trees_len == 0:
                self.log.warning('"{}" could not be parsed.'.format(sentence))
                answers.append((sentence, None))
            else:
                answers.append((sentence, trees[0]))
                if trees_len > 1:
                    self.log.warning('Ambguity alert! "{}" creates {} trees.'
                            .format(sentence, trees_len))
                    for i, tree in enumerate(trees):
                        self.log.warning('Tree {}:\n{}'.format(i, tree))
        return answers

    def desugar(self, raw_str):
        for key, value in contractions.items():
            raw_str = raw_str.replace(key, value)
        return raw_str

    def tokenize(self, raw_str):
        raw_str = raw_str.lower()
        sentence = re.sub(r'([.?!]$|[,;\':+\-/*=%]|(?<=\d)[pa]m)', r' \1 ',
                raw_str.strip(), flags=re.IGNORECASE)
        return sentence.split()
