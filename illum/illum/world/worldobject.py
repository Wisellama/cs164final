from uuid import uuid4

class WorldObject(dict):
    singular = ()
    plural = ()
    countable = True

    def __init__(self, uid=None, **kwargs):
        self.uuid = uid or uuid4()
        for key, val in kwargs.items(): #perhaps replace with update later?
            self[key] = val

    def __getitem__(self, key):
        if key == 'possessions' and key not in self:
            self[key] = set()
        val = dict.__getitem__(self, key)
        if isinstance(val, property):
            return val.fget()
        else:
            return val

    def __setitem__(self, key, val):
        current_val = dict.get(self, key)
        if isinstance(current_val, property):
            current_val.fset(val)
        else:
            dict.__setitem__(self, key, val)

    def __eq__(self, other):
        return type(self) == type(other) and self.uuid == other.uuid

    def __hash__(self):
        return hash(self.uuid)

    def steal_property(self, target, prop_name, rename=None):
        rename = rename or prop_name
        prop = getattr(type(target), prop_name)
        self[rename] = property(lambda: prop.fget(target),
                lambda v: prop.fset(target, v))

