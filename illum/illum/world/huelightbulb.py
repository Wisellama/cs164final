from illum.world import WorldObject

class HueLightBulb(WorldObject):
    singular = ('light', 'light bulb', 'hue')
    plural = ('lights', 'light bulbs', 'hues')

    def __init__(self, hue_bulb=None):
        WorldObject.__init__(self)
        if hue_bulb:
            self.steal_property(hue_bulb, 'on')
            self.steal_property(hue_bulb, 'xy')
            self.steal_property(hue_bulb, 'brightness')
            self.steal_property(hue_bulb, 'hue')
            self.steal_property(hue_bulb, 'saturation')
