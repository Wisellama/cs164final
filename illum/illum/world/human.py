from illum.world import WorldObject

class Human(WorldObject):
    singular = ('human', 'person')
    plural = ('humans', 'people')

    def __init__(self, full_name=None, gender=None, phone_number=None):
        WorldObject.__init__(self)
        #self['alive'] = True
        if full_name:
            self['full name'] = full_name.lower()
        if gender:
            self['gender'] = gender.lower()
        if phone_number:
            self['phone number'] = phone_number

    def generate_names(self):
        names = []
        if 'full name' in self:
            full_name = self['full name']
            names.append(full_name)
            split_name = full_name.split()
            name_len = len(split_name)
            if name_len > 1:
                names.append(split_name[0])
            if name_len > 2:
                names.append(split_name[0] + ' ' + split_name[-1])
        if 'nick names' in self:
            names.extend(self['nick names'])
        return names
