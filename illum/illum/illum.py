#!/usr/bin/env python3

import readline
import logging
logging.basicConfig(format='[%(name)s] %(levelname)s: %(message)s')

import phue
from illum import Parser, Conversation, ConvoDict
from illum.world import WorldObject, Human, HueLightBulb
from illum.test_bridge import TestBridge

class Illum(WorldObject):
    def __init__(self, world_objects=()):
        WorldObject.__init__(self)
        self.log = logging.getLogger('Illum')
        self.log.setLevel(logging.DEBUG)

        self.NP_table = {}
        self.human_table = {}
        self.phone_table = {}
        self.conversations = ConvoDict(self)
        self.parser = Parser(generate=False)
        self.learn(*world_objects)

    def learn(self, *world_objects):
        """ Inserts object names into Grammar
        Iterates over world objects and inserts their corresponding names into
        the grammar.
        """
        for obj in world_objects:
            if isinstance(obj, WorldObject):
                self.parser.add_production('COMMON[-plural]', obj.singular)
                self.parser.add_production('COMMON[+plural]', obj.plural)
                for w in obj.singular + obj.plural:
                    self.NP_table[w] = obj
                for thing in obj['possessions']:
                    self.parser.add_production('COMMON[-plural]', thing.singular)
                    self.parser.add_production('COMMON[+plural]', thing.plural)
                    for w in thing.singular + thing.plural:
                        self.NP_table[w] = thing
            if isinstance(obj, Human):
                names = obj.generate_names()
                for name in names:
                    self.human_table[name] = obj
                self.parser.add_production('PROPER', names)
                if 'phone number' in obj:
                    self.phone_table[obj['phone number']] = obj
        self.parser.generate_fcfg()

    def input(self, raw_str=None, speaker=None):
        convo = self.conversations[speaker]
        if raw_str is None:
            raw_str = input('> ')
        parsed = self.parser.parse(raw_str)
        responses = []
        for sentence, tree in parsed:
            self.log.debug('\nSentence: {}\nTree: {}'.format(sentence, tree))
            if tree is None:
                response = ('I\'m sorry. I could not parse the following '
                        'sentence: "{}"'.format(sentence))
            else:
                response = convo.evaluate(tree, speaker)
            responses.append(str(response))
        return ' '.join(responses)

    @staticmethod
    def get_lights(ip='0.0.0.0'):

        if ip == '0.0.0.0':
            bridge = TestBridge();
        else :
            bridge = phue.Bridge(ip)
            
        lights = []
        for bulb in bridge.lights:
            hue = HueLightBulb(bulb)
            for location in ('desk', 'chair', 'bed'):
                if location in bulb.name.lower():
                    hue['location'] = location
                    break
            lights.append(hue)
        return lights
        #return [HueLightBulb(bulb) for bulb in bridge.lights]
