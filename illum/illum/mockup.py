from collections import namedtuple
from re import compile as regex

class OR(list):
    def __init__(self, *args):
        self.extend(args)
OPT = namedtupe('Optional', 'optional')

Sentence = namedtuple('Sentence', ['main_clause', 'dep_clauses'])
Question = namedtuple('Question', ['subject', 'VP', 'dir_obj', 'indir_obj'])
Statement = namedtuple('Statement', ['subject', 'VP', 'dir_obj', 'indir_obj'])
Command = namedtuple('Command', ['subject', 'VP', 'dir_obj', 'indir_obj'])
NP = namedtuple('NP', ['det', 'adjs', 'noun'])
VP = namedtuple('VP', ['verb', 'tense', ])

proper_nouns = set('Ben', 'Sean', 'Eswar', 'Ras')
common_nouns = set('lights')

GRAMMARS = {
        Sentence: OR((MainClause, lambda g: Sentence(g, None)),
                     ([CondClause, MainClause], lambda g: Sentence(g[1], g[0])),
                     ([MainClause, CondClause], lambda g: Sentence(g[0], g[1]))),

        MainClause: (OR(Question,
                        Statement,
                        Command), lambda g: MainClause(g)),

        CondClause: ([OR('if', 'when'), Statement], lambda g: CondClause(g[1])),

        Question: OR(([AuxVerb, NP, OPT('not'), OR(BaseVerb, Participle)], lambda g: None),
                     ([Statement, '?'], lambda g: None)),

        Statement: OR(([Statement, OR('.', '!')], lambda g: g[0])
                      ([NP, VP, NP, 'to', NP], lambda g: Statement(g[0], g[1], g[2], g[4])),
                      ([NP, VP, NP, NP], lambda g: Statement(g[0], g[1], g[3], g[2])),
                      ([NP, VP, NP], lambda g: Statement(g[0], g[1], g[2], None)),
                      ([NP, VP], lambda g: Statement(g[0], g[1], None, None))),

        Command: [VP, NP],

        VP: OR(([OPT(AuxVerb), PhraseVerb, NP, OPT(Preposition)], lambda g: [VP(...), NP]),
               ([OPT(AuxVerb), PhraseVerb, OPT(Preposition)], lambda g: VP(...)),
               ([OPT(AuxVerb), BaseVerb], )),

        NP: OR((proper_nouns, lambda g: NP(None, None, g)),
                [determiner, OPT(adjective), common_nouns]),

        determiner: (OR('the',
                        'a',
                        'my',
                        'your'), lambda g: determiner(g)),

        # can I have a grammar that purely rearranges lexemes?
        # read one with intro in the title
}

DESUGAR = {'please': ''}
