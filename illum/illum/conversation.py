from nltk.featstruct import TYPE

class Conversation(list):
    def __init__(self, illum, a=None, b=None):
        self.illum = illum
        self.a = a
        self.b = b or illum

    def evaluate(self, tree, speaker):
        for node in tree:
            label = node.label()
            node_type = label[TYPE]
            if node_type == 'Clause':
                clause_type = label['type']
                if clause_type == 'command':
                    return self.eval_command(node)
                elif clause_type == 'question':
                    return self.eval_question(node)
                else:
                    return self.eval_statement(node)
        return ''

    def eval_NP(self, NP):
        if len(NP) >= 2:
            loot = []
            det = NP[0]
            common = NP[-1]
            if len(det) == 2:
                owner = self.eval_NP(det[0])
            else:
                word = det.leaves()[0]
                if word == 'your':
                    owner = self.b # bad practice
                else:
                    owner = self.a # bad practice
            target_str = common.leaves()[0]
            target = self.illum.NP_table[target_str]
            for pos in owner['possessions']:
                if isinstance(pos, type(target)):
                    loot.append(pos)
            if len(NP) == 3:
                adj_str = NP[1].leaves()[0]
                adj = self.illum.parser.adj_table[adj_str]
                filtered = []
                for thing in loot:
                    good = True
                    for key, val in adj.definition.items():
                        if thing[key] != val:
                            good = False
                    if good:
                        filtered.append(thing)
                return filtered
            else:
                return loot
        else:
            node = NP[0]
            label = node.label()
            node_type = label[TYPE]
            node_val = node.leaves()[0]
            if node_type == 'PROPER':
                return self.illum.human_table[node_val]
            else: #'PRONOUN':
                if node_val == 'you':
                    return self.b
                else:
                    return self.a

    def eval_VP(self, VP):
        verb_str = VP.leaves()[-1]
        return self.illum.parser.verb_table[verb_str]

    def eval_command(self, command):
        subject = self
        verb = None
        direct_object = None
        indirect_object = None
        adverb = None
        for node in command:
            label = node.label()
            node_type = label[TYPE]
            if node_type == 'VP':
                verb = self.eval_VP(node)
            elif node_type == 'NP':
                direct_object = self.eval_NP(node)
            elif node_type == 'ADV':
                adv_str = node.leaves()[0]
                adverb = self.illum.parser.adv_table[adv_str]
        verb_instance = verb(subject, direct_object, indirect_object, adverb)
        return verb_instance.perform()

    def eval_question(self, question):
        subject = None
        verb = None
        direct_object = None
        indirect_object = None
        adverb = None
        for node in question:
            label = node.label()
            node_type = label[TYPE]
            print('NODE TYPE:', node_type)
            if node_type == 'VP' or node_type == 'V':
                verb = self.eval_VP(node)
            elif node_type == 'NP':
                direct_object = self.eval_NP(node)
            elif node_type == 'ADV':
                adv_str = node.leaves()[0]
                adverb = self.illum.parser.adv_table[adv_str]
        verb_instance = verb(subject, direct_object, indirect_object, adverb)
        answer = verb_instance.validate()
        if answer:
            return 'Yes.'
        else:
            return 'No.'

    def eval_statement(self, statement):
        pass
