__version__ = '0.1'

from illum.parser import Parser
from illum.conversation import Conversation
from illum.convodict import ConvoDict
from illum.illum import Illum
