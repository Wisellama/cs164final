                                              # NOTE: this requires PyAudio because it uses the Microphone class
import speech_recognition as sr
r = sr.Recognizer()
with sr.Microphone() as source:                # use the default microphone as the audio source
    audio = r.listen(source)                   # listen for the first phrase and extract it into audio data

try:
    text = r.recognize(audio)
    print("You said " + text)    # recognize speech using Google Speech Recognition
    f = open("input.txt","w")
    f.write(text)
except LookupError:                            # speech is unintelligible
    print("Could not understand audio")
