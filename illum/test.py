#!/usr/bin/env python3
import sys
import readline
from pprint import pprint
from illum import Illum
from illum.world import Human
from illum.english.verbs import Be, Turn
from illum.english.adjectives import alive, dead
from illum.english.adverbs import off, on

lights = Illum.get_lights('0.0.0.0')

ben = Human('Benjamin Michael White', 'male', '8588770422')
ben['alive'] = True
ben['possessions'] |= set(lights)
#verb = Be(ben, alive)
#verb.perform()
#print(ben)

jaya = Human('Jaya Singh', 'female', '6618584760')

alex = Human('Alexander Igorivich Kozintsev', 'male', '4084251913')

ryan = Human('Ryan Adam Fish', 'male', '8189849977')

il = Illum([ben, jaya, alex, ryan])
il['possessions'] |= set(lights)

#verb = Turn(il, lights, adverb=on)
#print(Be(lights, adverb=on).validate())
#verb.perform()
#print(Be(lights, adverb=on).validate())
args = sys.argv[1:]
if args:
    answers = il.input(' '.join(args), ben)
    il.log.info(answers)
else:
    while True:
        answers = il.input(speaker=ben)
        il.log.info(answers)
